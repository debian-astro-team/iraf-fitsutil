# FITSUTIL external package

`fgwrite` encapsulates arbitrary files into wrapper FITS extensions
called FOREIGN.

## fgread, fgwrite

```
cl> fitsutil
cl> copy dev$pix.pix pix.pix
cl> copy dev$pix.imh pix.imh
cl> fgwrite "pix.pix pix.imh" pix.fits verb-
cl> mkdir out
cl> cd out
cl> fgread ../pix.fits "" "" verb-
cl> sum32 *
2024286706         5288 pix.imh
1504446139       526336 pix.pix
```

## fxcopy, fxinsert, fxheader

The functions in the `x_fitsutil.e` executable handle multi-extension
FITS files.

```
cl> fitsutil
cl> imcopy dev$pix spix
dev$pix -> spix
cl> fxcopy spix mpix.fits 0
spix[0] -> mpix.fits
cl> fxinsert spix mpix.fits[1] 0
spix[0] -> mpix.fits
cl> fxheader mpix.fits
EXT#  EXTTYPE             EXTNAME       EXTVE DIMENS     BITPI INH OBJECT       
0     mpix.fits                               512x512    16        m51  B  600s 
1      IMAGE                                  512x512    16        m51  B  600s 
```

## ricepack/funpack
    
The RICEPACK task will compress a list of images.  The input may be
FITS or MEF FITS, may be gzip compressed copies of either type of FITS
files, or may be IRAF ".imh" images.  The output file names will be
compressed FITS with ".fits.fz" appended. This task is a wrapper
script for the CFITSIO fpack command

```
cl> fitsutil
cl> copy spix.fits spix0.fits
cl> ricepack spix.fits verb-
cl> funpack spix.fits.fz verb-
cl> imstat spix*.fits
#               IMAGE      NPIX      MEAN    STDDEV       MIN       MAX
            spix.fits    262144     108.3     131.3       -1.    19936.
           spix0.fits    262144     108.3     131.3       -1.    19936.
```
